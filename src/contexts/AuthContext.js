import React, {Component, createContext} from 'react'

export const AuthContext = createContext()

class AuthContextProvider extends Component {
    state = {
        isAuthenticated: false,
    }

    toggleAuth = () => {
        this.setState((prevState) => ({
            isAuthenticated: !prevState.isAuthenticated 
        }))
    }

    render(){
        const {children} = this.props
        return (
            <AuthContext.Provider value={{ ...this.state, toggleAuth: this.toggleAuth }}>
                {children}
            </AuthContext.Provider>
        )
    }
}

export default AuthContextProvider
