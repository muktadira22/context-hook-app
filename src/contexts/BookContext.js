import React, {createContext, useState} from 'react'

export const BookContext = createContext()

const BookContextProvider = (props) => {
    const [books, setBooks] = useState([
        { title: 'Algoritma dan Pemrograman Dasar', id: 1 },
        { title: 'Pemahaman Tentang Koding', id: 2 },
        { title: 'Koding untung anak', id: 3 },
        { title: 'Become a Javascript Master', id: 4 },
    ])
    return (
        <BookContext.Provider value={{books}}>
            {props.children}
        </BookContext.Provider>
    )
}

export default BookContextProvider
